/**
 * Created by Treemont Haga on 7/1/2016.
 */
public class Item
{
    private String name;
    private int quantityValue;

    public Item(String newName, int newQuantityValue)
    {
        name = newName;
        quantityValue = newQuantityValue;
    }

    // Getters.
    public String getName()
    {
        return name;
    }
    public int getQuantityValue()
    {
        return quantityValue;
    }

    // Setters.
    public void setName(String newName)
    {
        name = newName;
    }
    public void setQuantityValue(int newQuantityValue)
    {
        quantityValue = newQuantityValue;
    }

    // toString.
    public String toString()
    {
        return name + ": " + quantityValue;
    }
}
