/**
 * Created by Treemont Haga on 6/30/2016.
 */
public class Monster
{
    private String name;
    private int health, attack, defense, speed, experience, bounty, killCount;

    public Monster(String newName, int newHealth, int newAttack, int newDefense, int newSpeed, int newExperience, int newBounty)
    {
        name = newName;
        health = newHealth;
        attack = newAttack;
        defense = newDefense;
        speed = newSpeed;
        experience = newExperience;
        bounty = newBounty;
        killCount = 1;
    }
    // Getters.
    public String getName()
    {
        return name;
    }
    public int getHealth()
    {
        return health;
    }
    public int getAttack()
    {
        return attack;
    }
    public int getDefense()
    {
        return defense;
    }
    public int getSpeed()
    {
        return speed;
    }
    public int getExperience()
    {
        return experience;
    }
    public int getBounty()
    {
        return bounty;
    }
    public int getKillCount()
    {
        return killCount;
    }
    // Setters.
    public void setName(String newName)
    {
        name = newName;
    }
    public void setHealth(int newHealth)
    {
        health = newHealth;
    }
    public void setAttack(int newAttack)
    {
        attack = newAttack;
    }
    public void setDefense(int newDefense)
    {
        defense = newDefense;
    }
    public void setSpeed(int newSpeed)
    {
        speed = newSpeed;
    }
    public void setExperience(int newExperience)
    {
        experience = newExperience;
    }
    public void setBounty (int newBounty)
    {
        bounty = newBounty;
    }
    public void setKillCount(int newKillCount)
    {
        newKillCount = killCount;
    }
    // toString.
    public String toString()
    {
        return name + ": " + killCount;
    }
}
