import java.util.Random;
/**
 * Created by Treemont Haga on 6/30/2016.
 */
public class Slime extends Monster
{
    public Slime()
    {
        super("Slime", 30, 20, 0, 0, 50, 50);
    }
    public Item getDrop()
    {
        Item drop = null;
        final int LOWER_BOUND = 1;
        final int RARE_LOWER_BOUND = 1;
        final int UPPER_BOUND = 100;
        final int RARE_UPPER_BOUND = 2;
        Random random = new Random();
        int randomNumber = random.nextInt(UPPER_BOUND - LOWER_BOUND + 1) + LOWER_BOUND;
        if (randomNumber == 1)
        {
            int rareRandomNumber = random.nextInt(RARE_UPPER_BOUND - RARE_LOWER_BOUND + 1) + RARE_LOWER_BOUND;
            if (rareRandomNumber == 1)
            {
                drop = new Item("Miniature Slime", 1);
            }
            if (rareRandomNumber == 2)
            {
                drop = new Item("Slime Mask", 1);
            }
        }
        else
        {
            drop = new Item("Glob of Slime", 1);
        }
        return drop;
    }
}
