/**
 * Created by Treemont Haga on 6/30/2016.
 */
public class Driver
{
    public static void main(String[] args)
    {
        Controller controller = new Controller();
        controller.runGame();
    }
}
