import java.util.ArrayList;
/**
 * Created by Treemont Haga on 6/30/2016.
 */
public class Player
{
    private String name;
    private int level, health, attack, defense, speed, experience, experienceNeeded, coinCount;
    private ArrayList<Item> inventory;

    public Player(String newName)
    {
        name = newName;
        level = 1;
        health = 100;
        attack = 10;
        defense = 10;
        speed = 10;
        experience = 0;
        experienceNeeded = 100;
        coinCount = 0;
        inventory = new ArrayList<>();
    }

    // Getters.
    public String getName()
    {
        return name;
    }
    public int getLevel()
    {
        return level;
    }
    public int getHealth()
    {
        return health;
    }
    public int getAttack()
    {
        return attack;
    }
    public int getDefense()
    {
        return defense;
    }
    public int getSpeed()
    {
        return speed;
    }
    public int getExperience()
    {
        return experience;
    }
    public int getExperienceNeeded()
    {
        return experienceNeeded;
    }
    public int getCoinCount()
    {
        return coinCount;
    }

    // Setters.
    public void setName(String newName)
    {
        name = newName;
    }
    public void setLevel(int newLevel)
    {
        level = newLevel;
    }
    public void setHealth(int newHealth)
    {
        health = newHealth;
    }
    public void setAttack(int newAttack)
    {
        attack = newAttack;
    }
    public void setDefense(int newDefense)
    {
        defense = newDefense;
    }
    public void setSpeed(int newSpeed)
    {
        speed = newSpeed;
    }
    public void setExperience(int newExperience)
    {
        experience = newExperience;
    }
    public void setExperienceNeeded(int newExperienceNeeded)
    {
        experienceNeeded = newExperienceNeeded;
    }
    public void setCoinCount(int newCoinCount)
    {
        coinCount = newCoinCount;
    }

    // Inventory.
    public String viewInventory()
    {
        String inventoryList = "";
        if (inventory.size() > 0)
        {
            for (int index = 0; index < inventory.size(); index++)
            {
                if (index == inventory.size() - 1)
                {
                    inventoryList += inventory.get(index);
                }
                else
                {
                    inventoryList += inventory.get(index) + "\n";
                }
            }
        }
        else
        {
            inventoryList = "There appears to be nothing in your inventory.";
        }
        return inventoryList;
    }
    public void addToInventory(Item newItem)
    {
        inventory.add(newItem);
    }
    public void addToExistingItem(Item newItem)
    {
        for (int index = 0; index < inventory.size(); index++)
        {
            if (newItem.getName().equals(inventory.get(index).getName()))
            {
                inventory.get(index).setQuantityValue(inventory.get(index).getQuantityValue() + 1);
            }
        }
    }
    public boolean searchInventory(String itemName)
    {
        boolean isFound = false;
        for (int index = 0; index < inventory.size(); index++)
        {
            if (itemName.equals(inventory.get(index).getName()))
            {
                isFound = true;
            }
        }
        return isFound;
    }

    // toString.
    public String toString()
    {
        return "Name: " + name +
                "\nLevel: " + level +
                "\nHealth: " + health +
                "\nAttack: " + attack +
                "\nDefense: " + defense +
                "\nSpeed: " + speed +
                "\nExperience: " + experience + "/" + experienceNeeded +
                "\nCoins: " + coinCount;
    }
}
