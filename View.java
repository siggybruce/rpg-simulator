import java.util.Scanner;
/**
 * Created by Treemont Haga on 6/30/2016.
 */
public class View
{
    private Player player;

    public View()
    {
        player = new Player("");
    }
    // Greeting options.
    public boolean greetingOptions()
    {
        System.out.println("Welcome to the text-based RPG!");
        boolean choice = false;
        boolean invalidOption = true;
        while (invalidOption)
        {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Would you like to start a new game? (Yes[Y] | No[N]): ");
            String input = scanner.nextLine().toUpperCase();
            switch (input)
            {
                case "Y":
                    choice = true;
                    invalidOption = false;
                    break;
                case "N":
                    System.out.println("Perhaps another time...");
                    choice = false;
                    invalidOption = false;
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
                    invalidOption = true;
            }
        }
        return choice;
    }
    // Character creation options.
    public void characterCreationOptions()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a name for your character: ");
        String name = scanner.nextLine();
        player.setName(name);
        System.out.println("Your character has been created! Enjoy!");
    }
    // Game options.
    public boolean gameOptions()
    {
        boolean choice = false;
        boolean invalidOption = true;
        while (invalidOption)
        {
            Scanner scanner = new Scanner(System.in);
            System.out.print("What would you like to do " + player.getName() + "? (Hunt[H] | Inventory[I] | Character[C] | Quit[Q]): ");
            String input = scanner.nextLine().toUpperCase();
            switch (input)
            {
                case "H":
                    huntMode();
                    choice = false;
                    invalidOption = false;
                    break;
                case "I":
                    System.out.println(player.viewInventory());
                    choice = false;
                    invalidOption = false;
                    break;
                case "C":
                    System.out.println(player);
                    choice = false;
                    invalidOption = false;
                    break;
                case "Q":
                    System.out.println("Perhaps another time...");
                    choice = true;
                    invalidOption = false;
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
                    invalidOption = true;
            }
        }
        return choice;
    }
    // Hunt mode.
    private void huntMode()
    {
        final int LEVEL = 1;
        final int HEALTH = 100;
        Monster monster;
        Item drop;

        // Generates a new monster and generates a monster drop.
        if (player.getLevel() <= 2)
        {
            Slime slime = new Slime();
            drop = slime.getDrop();
            monster = slime;
        }
        else
        {
            Goblin goblin = new Goblin();
            drop = goblin.getDrop();
            monster = goblin;
        }

        // Adds the monster drop to the player's inventory.
        if (!player.searchInventory(drop.getName()))
        {
            player.addToInventory(drop);
        }
        else
        {
            player.addToExistingItem(drop);
        }

        // Displays the result of the battle.
        System.out.println("After a rigorous battle, you slay a " + monster.getName() + ".");
        System.out.println("You gain " + monster.getExperience() + " experience points and " + monster.getBounty() + " coins!");
        System.out.println("You have received drop: " + drop.getName() + ".");

        // Player gains.
        player.setCoinCount(player.getCoinCount() + monster.getBounty());
        player.setExperience(player.getExperience() + monster.getExperience());
        if (player.getExperience() >= player.getExperienceNeeded())
        {
            player.setLevel(player.getLevel() + LEVEL);
            System.out.println("Congratulations " + player.getName() + "! You are now level " + player.getLevel() + "!");
            player.setExperience(player.getExperience() - player.getExperienceNeeded());
            player.setExperienceNeeded(player.getExperienceNeeded() * 2);
            player.setHealth(player.getHealth() + HEALTH);
        }
    }
}
