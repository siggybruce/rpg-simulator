/**
 * Created by Treemont Haga on 6/30/2016.
 */
public class Controller
{
    private View view;
    private boolean quitGame;

    public Controller()
    {
        view = new View();
        quitGame = false;
    }
    public void runGame()
    {
        while (!quitGame)
        {
            if (view.greetingOptions())
            {
                view.characterCreationOptions();
                while (!view.gameOptions())
                {
                    quitGame = true;
                }
            }
            else
            {
                quitGame = true;
            }
        }
    }
}
